
const redis = require("redis");
      
const client = redis.createClient({
    host: 'redis'
});

let err_callbak = (err) => {
    console.log(err);
    client.removeListener('error',err_callbak);
    client.end(true);
}
client.on('error', err_callbak);

client.set("foo_rand000000000000", "OKTest");
client.get("foo_rand000000000000", function(err, reply) {
    console.log(reply.toString()); // Will print `OK`
    client.end(true);
});
