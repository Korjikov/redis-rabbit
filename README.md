# Пример подключений к redis и rabbitmq

CI/CD и два файла с тестами

## Redis

### HSET-HGETALL
Добавление в hash-set едининой записи, поиск единичной записи

### SET-GET
Добавления и поиск для штучного ключа

### HSET-HGETALL-99
Добалвение 99 записей последовательно, поиск единичной записи

### HSET-HGETALL-multi add 20x10k
Добалвение 200000 строковых записей через multi

### HSET-HGET-single key99999=val99999
Поиск единичной записи

### HSET-HGETALL-multi add buffer 20x10k
Добалвение 200000 бинарных записей через multi

### HSCAN foo_rand000000000000 0 MATCH key1*1*
Поиск по регулярному выражению

### DEL all
Удаление всех данных из базы


## RabbitMQ

### rabbit-send-recv
Отправка тестового сообщения и прием тестового сообщения

### rabbitmq-send
Отправка тестового сообщения

### rabbitmq-receive
Прием тестового сообщения


![test results](image1.png "Test results")

