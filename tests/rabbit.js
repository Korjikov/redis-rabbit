
var open1 = require('amqplib').connect('amqp://rabbitmq');
var open2 = require('amqplib').connect('amqp://rabbitmq');

describe('#rabbit', function () {

    it('rabbit-send-recv', function (done) {

        var q = 'tasks';

        var open = require('amqplib').connect('amqp://rabbitmq');
        let _conn1;
        let _conn2;
        open.then(function(conn) {
            _conn1 = conn;
            return conn.createChannel();
        }).then(function(ch) {
        return ch.assertQueue(q).then(function(ok) {
            let msg = 'something to do';
            console.log(" [x] Sent '%s'", msg);
            return ch.sendToQueue(q, Buffer.from(msg));
        });
        }).catch(console.warn);

        // Consumer
        open.then(function(conn) {
            _conn2 = conn;
            return conn.createChannel();
        }).then(function(ch) {
        return ch.assertQueue(q).then(function(ok) {
            return ch.consume(q, function(msg) {
            if (msg !== null) {
                console.log(" [x] Received '%s'", msg.content.toString());
                ch.ack(msg);
                _conn1.close();
                _conn2.close();
                clearTimeout(timeoutObj);
                done();
            }
            });
        });
        }).catch(console.warn);

        const timeoutObj = setTimeout(() => {
            console.log(" recvMessage1 close conns ", new Date() );
            _conn1.close();
            _conn2.close();
            done(-1);
        }, 7000);

    }).timeout(10000);


    it('rabbitmq-send', function (done) {

        var q = 'tasks';
        let _conn;
        open1.then(function(conn) {
            _conn = conn;
            return conn.createChannel();
        }).then(function(ch) {
            return ch.assertQueue(q).then(function(ok) {
                let msg = 'something to do';
                console.log(" [x] Sent3 '%s'", msg);
                return ch.sendToQueue(q, Buffer.from(msg));
            }).then(function(err,ok){
                _conn.close();
                // conng2.close();
                clearTimeout(timeoutObj);
                done();
            });
        }).catch(console.warn)

        const timeoutObj = setTimeout(() => {
            console.log(" recvMessage1 close conns ", new Date() );
            _conn.close();
            done(-1);
        }, 7000);

    }).timeout(10000);





    it('rabbitmq-receive', function (done) {

        var q = 'tasks';

        let _conn;
        open2.then(function(conn) {
            _conn = conn;
            return conn.createChannel();
        }).then(function(ch) {
        return ch.assertQueue(q).then(function(ok) {
            return ch.consume(q, function(msg) {
            if (msg !== null) {
                console.log(" [x] Received3 '%s'", msg.content.toString());
                ch.ack(msg);
                _conn.close();
                clearTimeout(timeoutObj);
                done();
            }
            });
        });
        }).catch(console.warn);

        const timeoutObj = setTimeout(() => {
            console.log(" recvMessage3 close conns ", new Date() );
            _conn.close();
            done(-1);
        }, 7000);

    }).timeout(10000);





});
