
const assert = require('assert');

const redis1 = require("redis");
const redis2 = require("redis");
const redis3 = require("redis");

let redis_connect_str = {
    host: 'redis'
};

describe('#Redis', function () {


  it('HSET-HGETALL', function (done) {

    const redis = require("redis");
    
    const client = redis.createClient(redis_connect_str);
    
    const timeoutObj = setTimeout(() => {
        console.log(" recvMessage1 close conns ", new Date() );
        client.end(true);
        done(0);
    }, 4500);
    
    let err_callbak = (err) => {
      client.removeListener('error',err_callbak);
    }
    client.on('error', err_callbak);
    
    client.HSET("foo_rand000000000000", "f", "val");
    client.HGETALL("foo_rand000000000000", function(err, reply) {
        clearTimeout(timeoutObj)
        client.end(true);
        done();
    });
    
  }).timeout(5000);
  
  
  
  it('set-get', function (done) {

    const redis = require("redis");
    
    const client = redis.createClient(redis_connect_str);
    
    const timeoutObj = setTimeout(() => {
        console.log(" recvMessage1 close conns ", new Date() );
        client.end(true);
        done(0);
    }, 4500);
    
    let err_callbak = (err) => {
      client.removeListener('error',err_callbak);
    }
    client.on('error', err_callbak);
    
    client.set("foo_rand000000000001", "f");
    client.get("foo_rand000000000001", function(err, reply) {
        clearTimeout(timeoutObj)
        client.end(true);
        done();
    });
    
  }).timeout(5000);
  
  
  
  it('HSET-HGETALL-99', function (done) {

    const redis = require("redis");
    
    const client = redis.createClient(redis_connect_str);
    
    const timeoutObj = setTimeout(() => {
        console.log(" recvMessage1 close conns ", new Date() );
        client.end(true);
        done(0);
    }, 4500);
    
    let err_callbak = (err) => {
      client.removeListener('error',err_callbak);
    }
    client.on('error', err_callbak);
    
    for (let i = 0; i < 99 ; i++ ) {
        let k = "key" + i;
        client.HSET("foo_rand000000000000", k, "val");
    }
    
    client.HGETALL("foo_rand000000000000", function(err, reply) {
        clearTimeout(timeoutObj)
        client.end(true);
        done();
    });
    
  }).timeout(5000);
  


  it('HSET-HGETALL-multi add 20x10k', function (done) {

    const redis = require("redis");
    
    const client = redis.createClient(redis_connect_str);
    
    const timeoutObj = setTimeout(() => {
        console.log(" recvMessage1 close conns ", new Date() );
        client.end(true);
        done(0);
    }, 14500);
    
    let err_callbak = (err) => {
      client.removeListener('error',err_callbak);
    }
    client.on('error', err_callbak);
    
    for (let j = 0 ; j < 20 ; j++) {
        let ok = client.multi();
        for (let i = 0; i < 10000 ; i++ ) {
            let k = j * 10000 + i;
            ok = ok.HSET("foo_rand000000000000", "key"+k, "val"+k);
        }
        ok.exec();
    }
    
    client.HGETALL("foo_rand000000000000", function(err, reply) {
      
        clearTimeout(timeoutObj)
        client.end(true);
        done();
    });
    
  }).timeout(15000);
  
  
  
  it('HSET-HGET-single key99999=val99999', function (done) {

    const redis = require("redis");
    
    const client = redis.createClient(redis_connect_str);
    
    const timeoutObj = setTimeout(() => {
        console.log(" recvMessage1 close conns ", new Date() );
        client.end(true);
        done(0);
    }, 14500);
    
    let err_callbak = (err) => {
      client.removeListener('error',err_callbak);
    }
    client.on('error', err_callbak);
    
    client.HGET("foo_rand000000000000", 'key99999', function(err, reply) {
      
        clearTimeout(timeoutObj)
        client.end(true);
        done();
    });
    
  }).timeout(15000);
  
  
  
  it('HSET-HGETALL-multi add buffer 20x10k', function (done) {

    const redis = require("redis");
    
    const client = redis.createClient(redis_connect_str);
    
    const timeoutObj1 = setTimeout(() => {
        console.log(" recvMessage1 close conns ", new Date() );
        client.end(true);
        done(0);
    }, 14500);
    
    let err_callbak = (err) => {
      client.removeListener('error',err_callbak);
    }
    client.on('error', err_callbak);
    
    for (let j = 0 ; j < 20 ; j++) {
        let ok = client.multi();
        for (let i = 0; i < 10000 ; i++ ) {
            let k = j * 10000 + i;
            ok = ok.HSET("test200k_buffer", "key"+k, Buffer.from("val"+k));
        }
        ok.exec();
    }
    
    client.HGET(
        // commandOptions({ returnBuffers: true }), 
        "test200k_buffer", Buffer.from("key75444"), function(err, reply) {
        console.log("HSET-HGETALL reply", reply);

        clearTimeout(timeoutObj1)
        client.end(true);
        done();
    });
    
  }).timeout(15000);


  it('HSCAN foo_rand000000000000 0 MATCH key1*1*', function (done) {

    const redis = require("redis");
    
    const client = redis.createClient(redis_connect_str);
    
    const timeoutObj1 = setTimeout(() => {
        console.log(" recvMessage1 close conns ", new Date() );
        client.end(true);
        done(0);
    }, 14500);
    
    let err_callbak = (err) => {
      client.removeListener('error',err_callbak);
    }
    client.on('error', err_callbak);
    
    client.HSCAN(
        // commandOptions({ returnBuffers: true }), 
        "foo_rand000000000000", "0", "MATCH", "key1*1*", function(err, reply) {
        // console.log("HSCAN reply", reply);
        // console.log("HSCAN reply length", reply.length);

        clearTimeout(timeoutObj1)
        client.end(true);
        done();
    });
    
  }).timeout(15000);


  it('DEL all', function (done) {

    const redis = require("redis");
    
    const client = redis.createClient(redis_connect_str);
    
    const timeoutObj = setTimeout(() => {
        console.log(" recvMessage1 close conns ", new Date() );
        client.end(true);
        done(0);
    }, 4500);
    
    let err_callbak = (err) => {
      client.removeListener('error',err_callbak);
    }
    client.on('error', err_callbak);
    
    let cnt = 0;
    let cnt_all = 3;
    client.DEL("foo_rand000000000000", function(err, reply) {
        // console.log("HSET-HGETALL reply", reply);
        cnt++;
        if (cnt >= cnt_all) {
            clearTimeout(timeoutObj)
            client.end(true);
            done();
        }
    });
    client.DEL("foo_rand000000000001", function(err, reply) {
        // console.log("HSET-HGETALL reply", reply);
        cnt++;
        if (cnt >= cnt_all) {
            clearTimeout(timeoutObj)
            client.end(true);
            done();
        }
    });
    client.DEL("test200k_buffer", function(err, reply) {
        // console.log("HSET-HGETALL reply", reply);
        cnt++;
        if (cnt >= cnt_all) {
            clearTimeout(timeoutObj)
            client.end(true);
            done();
        }
    });
    
  }).timeout(5000);

});

